﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;

namespace TSAutotests
{
    class CloseAllPositionsDialog : IDisposable
    {
        private const string CLOSE_ALL_POSITIONS_DIALOG_NAME = "Close All Positions";
        private const string OK_BUTTON = "OK";

        private Dialog dialog;

        public CloseAllPositionsDialog(TradingStationWindow application)
        {
            dialog = Waiter.WaitDialogAppear(application, CLOSE_ALL_POSITIONS_DIALOG_NAME);
        }

        public void PressOK()
        { 
            dialog[OK_BUTTON].Click();
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialog.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
