﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using TSAutotests.Helpers.Tab;

namespace TSAutotests
{
    class LoginDialog : IDisposable
    {
        private const String LOGIN_DIALOG = "Login:";
        private const String PASSWORD_DIALOG = "Password:";
        private const String DIALOG_NAME_WINDOW = "Login";
        private const String LOGIN_BUTTON = "Login";
        private const String PLEASE_WAIT_NAME_WINDOW = "Please wait...";
        private const String MAIN_DIALOG_APPLICATION_NAME_WINDOW = "FXCM Trading Station (Dev)";

        private Dialog dialog;

        public LoginDialog(TradingStationWindow application)
        {
            dialog = application.GetDialog(DIALOG_NAME_WINDOW);
            if (dialog == null)
            {
                var menuLogin = new ApplicationMenuHelper(application);
                menuLogin.PressLogin();

                Waiter.WaitingCondition dialogExistsCondition = () => (application.GetDialog(DIALOG_NAME_WINDOW)) != null;
                if (!Waiter.Wait(dialogExistsCondition))
                {
                    throw new Exception("not find window login");
                }
                else
                {
                    dialog = application.GetDialog(DIALOG_NAME_WINDOW);
                }
            }
        }

        public bool Login(string login, string password, TradingStationWindow application, int timeout = 10)
        {
            dialog[LOGIN_DIALOG].Text = login;
            dialog[PASSWORD_DIALOG].StrokeA(password);
            dialog[LOGIN_BUTTON].Click();
            if (WaitLoginProccesing(timeout, application))
            {
                if (CheckLogon(application))
                {
                    return WaitAccountIsReady(application);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private static bool WaitLoginProccesing(int timeout, Window application)
        {
            DateTime localDate = DateTime.Now;
            Dialog dlg = null;

            dlg = Waiter.WaitDialogAppear(application, PLEASE_WAIT_NAME_WINDOW, timeout);

            if (dlg != null)
            {
                Waiter.WaitingCondition dialogExistsCondition = () => !dlg.Exists;
                if (Waiter.Wait(dialogExistsCondition))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        private bool WaitAccountIsReady(TradingStationWindow application)
        {
            var accountsTab = new AccountsTab(application);
            Waiter.WaitingCondition condition = () => accountsTab.CheckTabIsReady();
            return Waiter.Wait(condition, 20);
        }

        private static bool CheckLogon(Window application)
        {
            Dialog dlgInCorrect = application.GetDialog(MAIN_DIALOG_APPLICATION_NAME_WINDOW);
            if (dlgInCorrect == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialog.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
