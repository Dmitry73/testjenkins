﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System.Collections.Generic;
using System.Threading;


namespace TSAutotests
{
    class ManageSymbolSubscriptionDialog
    {
        private const String MANAGE_SYMBOL_SUBSCRIPTION_NAME_WINDOW = "Manage Symbol Subscription";

        private Dialog dialog;

        public ManageSymbolSubscriptionDialog (TradingStationWindow application)
        {
            dialog = application.GetDialog(MANAGE_SYMBOL_SUBSCRIPTION_NAME_WINDOW);
            if (dialog == null)
            {
                var applicationMenu = new ApplicationMenuHelper(application);
                Thread.Sleep(2000);
                applicationMenu.PressTradingDealingRatesManageSymbol();

                Waiter.WaitingCondition dialogExistsCondition = () => (application.GetDialog(MANAGE_SYMBOL_SUBSCRIPTION_NAME_WINDOW)) != null;
                if (!Waiter.Wait(dialogExistsCondition,20))
                {
                    throw new Exception("not find window Manage Symbol Subscription");
                }
                else
                {
                    dialog = application.GetDialog(MANAGE_SYMBOL_SUBSCRIPTION_NAME_WINDOW);
                }
            }
        }

        public void leaveOnlyThoseCurrenctyPairs (List<string> currancyPairs)
        {
            using (TreeViewHelper treeManageSymbol = new TreeViewHelper(dialog, ".*"))
            {
                foreach (string currancyPair in currancyPairs)
                {
                    TreeViewItem findItem = treeManageSymbol.FindElement(currancyPair);
                    findItem.Click(InputEventModifiers.LeftButton);
                    if (dialog[">"].Enabled)
                    {
                        dialog[">"].Click();
                    }
                }
                ListView manageSymbolView = (dialog.FindChild("SysListView32", ".*", true)) as ListView;
                int countRowManageSymbolView = manageSymbolView.RowCount;
                for (int i = 0; i < countRowManageSymbolView; i++)
                {
                    string valueRow = manageSymbolView.CellValue(i, 0);
                    if (!currancyPairs.Contains(valueRow))
                    {
                        manageSymbolView.ClickCell(i, 0, InputEventModifiers.LeftButton);
                    }
                }
                if (countRowManageSymbolView > currancyPairs.Count)
                {
                    Waiter.WaitingCondition availabilityButton = () => dialog["<"].Enabled;
                    if (!Waiter.Wait(availabilityButton))
                    {
                        throw new Exception("Problem button \"<\" in dialog window Manage Symbol Subscription");

                    }
                    else
                    {
                        dialog["<"].Click();
                    }
                }
                dialog["Close"].Click();
            }
        }


    }
}
