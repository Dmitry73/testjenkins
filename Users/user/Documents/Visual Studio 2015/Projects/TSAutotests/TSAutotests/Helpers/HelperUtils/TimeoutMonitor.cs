﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSAutotests
{
    class TimeoutMonitor
    {
        private DateTime endTime;

        public TimeoutMonitor(int timeout = 10)
        {
            endTime = DateTime.Now.AddSeconds(timeout);
        }

        public bool checkTimeout()
        {
            return (endTime < DateTime.Now);
        }
    }
}
