﻿using gehtsoft.applicationcontroller;
using System;
using System.Threading;

namespace TSAutotests
{
    static class Waiter
    {
        public delegate bool WaitingCondition();
        public delegate IDisposable DisposableWaitCondition();

        public static bool Wait(WaitingCondition condition, int timeout = 10)
        {
            var monitor = new TimeoutMonitor(timeout);
            while (!monitor.checkTimeout())
            {
                if (condition())
                {
                    return true;
                }
                Thread.Sleep(100);
            }
            return false;
        }

        private static bool DisposeConditionExecutor(DisposableWaitCondition condition)
        {
            using (IDisposable disposable = condition())
            {
                if (disposable != null)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool DisposableWait(DisposableWaitCondition disposeCondition, int timeout = 10)
        {
            WaitingCondition condition = () => DisposeConditionExecutor(disposeCondition);
            return Wait(condition, timeout);
        }

        public static Dialog WaitDialogAppear(Window app, string title, int timeout = 10)
        {
            DisposableWaitCondition disposableCondition = () => app.GetDialog(title);
            if (DisposableWait(disposableCondition, timeout))
                return app.GetDialog(title);
            else
            {
                return null;
            }
        }

    }
}
