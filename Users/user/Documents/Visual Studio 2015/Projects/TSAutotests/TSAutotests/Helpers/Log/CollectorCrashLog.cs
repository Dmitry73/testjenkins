﻿using NUnit.Framework;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System.Threading;
using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;

namespace TSAutotests
{
    class CollectorLog
    {
        private const string PATH_FOLDER_CRASH = @"C:\Program Files (x86)\Candleworks\FXTS2\crash";
        private const string PATH_FOLDER_LOG = @"C:\Program Files (x86)\Candleworks\FXTS2\log";
        private const string PATH_TO_STORE_ERROR= @".\Errors crash";

        public void ArchivingLogsFall ()
        {
            if (!Directory.Exists(PATH_TO_STORE_ERROR))
            {
                Directory.CreateDirectory(PATH_TO_STORE_ERROR);
            }

            int countFile = 0;
            string[] foldersWithRecordCrash;

            if (Directory.Exists(PATH_FOLDER_CRASH))
            {
                foldersWithRecordCrash = Directory.GetDirectories(PATH_FOLDER_CRASH);
                countFile = 7;
            }
            else
            {
                foldersWithRecordCrash = Directory.GetDirectories(PATH_FOLDER_LOG);
                countFile = 19;
            }

            foreach (string folderWithRecordCrash in foldersWithRecordCrash)
            {
                try
                {
                    Waiter.WaitingCondition condition = () => Directory.GetFiles(folderWithRecordCrash).Length == countFile;
                    if (!Waiter.Wait(condition))
                        throw new Exception("Not found needed crash reporting files");

                    ZipFile.CreateFromDirectory(folderWithRecordCrash,
                        "crash" + DateTime.Now.ToShortDateString() + DateTime.Now.Hour +
                        DateTime.Now.Minute + DateTime.Now.Second + ".zip");
                }
                catch (Exception e)
                {

                }
            }
        }
    }
}
