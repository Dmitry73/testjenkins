﻿using System;
using System.IO;
using System.Text;

namespace TSAutotests
{
    public static class Log
    {
        public static void WriteLog(string informationLog, string pathLog = "./")
        {
            try
            {
                int log_size = 100000;
                FileStream fs1 = new FileStream(pathLog + "log.txt", FileMode.Append);
                long lenght = fs1.Length;
                fs1.Dispose();
                if (lenght >= log_size)
                {
                    DateTime timeNow = DateTime.Now;
                    File.Move(pathLog + "log.txt", pathLog + "log_" + timeNow.ToShortDateString() + "." + timeNow.Hour + "." + timeNow.Minute + "." + timeNow.Second + @".old");
                }
                FileStream fs2 = new FileStream(pathLog + "log.txt", FileMode.Append);
                StreamWriter sw = new StreamWriter(fs2);
                DateTime timeNowRecord = DateTime.Now;
                sw.WriteLine(timeNowRecord.ToString() + ":"+timeNowRecord.Millisecond+ ": " + informationLog);
                sw.WriteLine(" ");
                sw.Close();
                fs2.Dispose();
            }
            catch (Exception ex)
            {

            }
        }
    }
}