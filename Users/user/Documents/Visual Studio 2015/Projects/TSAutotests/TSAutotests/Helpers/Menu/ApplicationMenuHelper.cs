﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;

namespace TSAutotests
{
    class ApplicationMenuHelper : MenuHelper
    {
        private WindowMenu menu;

        public ApplicationMenuHelper(TradingStationWindow application)
        {
            menu = application.GetMenu();
        }

        public void PressLogin()
        {
            WindowMenuItem menuSystem = findSubMenuItemByName(menu, "System");
            WindowMenuItem menuItemLogin = findSubMenuItemByName(menuSystem.Submenu, "Login...");
            int pressLogin = menuItemLogin.Command;
            menu.Window.SendCommand(pressLogin);
        }

        public void PressViewOrders(bool setChecked = true)
        {
            WindowMenuItem menuView = findSubMenuItemByName(menu, "View");
            WindowMenuItem menuItemOrders = findSubMenuItemByName(menuView.Submenu, "Orders");
            if (!menuItemOrders.Checked == setChecked)
            {
                int pressOrders = menuItemOrders.Command;
                menu.Window.SendCommand(pressOrders);
            }
        }

        public void PressViewSummary(bool setChecked = true)
        {
            WindowMenuItem menuView = findSubMenuItemByName(menu, "View");
            WindowMenuItem menuItemSummary = findSubMenuItemByName(menuView.Submenu, "Summary");
            if (!menuItemSummary.Checked == setChecked)
            {
                int pressSummary = menuItemSummary.Command;
                menu.Window.SendCommand(pressSummary);
            }
        }

        public void PressRemoveOrder()
        {
            WindowMenuItem menuTrading = findSubMenuItemByName(menu, "Trading");
            WindowMenuItem menuItemOrders = findSubMenuItemByName(menuTrading.Submenu, "Orders");
            WindowMenuItem menuRemoveOrders = findSubMenuItemByName(menuItemOrders.Submenu, "Remove Order");
            int pressRemoveOrder = menuRemoveOrders.Command;
            menu.Window.SendCommand(pressRemoveOrder);
        }

        public void PressOpenCreateContingentOrder()
        {
            WindowMenuItem menuTrading = findSubMenuItemByName(menu, "Trading");
            WindowMenuItem menuItemOrders = findSubMenuItemByName(menuTrading.Submenu, "Orders");
            WindowMenuItem menuCreateContingentOrder = findSubMenuItemByName(menuItemOrders.Submenu, "Create Contingent Order...");
            int pressCreateContingentOrder = menuCreateContingentOrder.Command;
            menu.Window.SendCommand(pressCreateContingentOrder);
        }

        public void PressCloseAllPositions()
        {
            WindowMenuItem menuTrading = findSubMenuItemByName(menu, "Trading");
            WindowMenuItem menuItemOpenPositions = findSubMenuItemByName(menuTrading.Submenu, "Open Positions");
            WindowMenuItem menuCloseAllPosions = findSubMenuItemByName(menuItemOpenPositions.Submenu, "Close All Positions...");
            int pressCloseAllPositions = menuCloseAllPosions.Command;
            menu.Window.SendCommand(pressCloseAllPositions);
        }

        public void PressViewDefault()
        {
            WindowMenuItem menuView = findSubMenuItemByName(menu, "View");
            WindowMenuItem menuItemDefault = findSubMenuItemByName(menuView.Submenu, "Default Layout");
            int pressDefault = menuItemDefault.Command;
            menu.Window.SendCommand(pressDefault);
        }

        public void PressViewSymbolList(bool setChecked = true)
        {
            WindowMenuItem menuView = findSubMenuItemByName(menu, "View");
            WindowMenuItem menuItemSymbol = findSubMenuItemByName(menuView.Submenu, "Symbol List");
            if (!menuItemSymbol.Checked == setChecked)
            {
                int pressSymbol = menuItemSymbol.Command;
                menu.Window.SendCommand(pressSymbol);
            }
        }

        public void PressViewOpenPositions(bool setChecked = true)
        {
            WindowMenuItem menuView = findSubMenuItemByName(menu, "View");
            WindowMenuItem menuItemOpenPositions = findSubMenuItemByName(menuView.Submenu, "Open Positions");
            if (!menuItemOpenPositions.Checked == setChecked)
            {
                int pressCommand = menuItemOpenPositions.Command;
                menu.Window.SendCommand(pressCommand);
            }
        }

        public void PressViewAccounts(bool setChecked = true)
        {
            WindowMenuItem menuView = findSubMenuItemByName(menu, "View");
            WindowMenuItem menuItemOpenPositions = findSubMenuItemByName(menuView.Submenu, "Accounts");
            if (!menuItemOpenPositions.Checked == setChecked)
            {
                int pressCommand = menuItemOpenPositions.Command;
                menu.Window.SendCommand(pressCommand);
            }
        }

        public void PressSystemExit()
        {
            WindowMenuItem menuSystem = findSubMenuItemByName(menu, "System");
            WindowMenuItem menuItemExit = findSubMenuItemByName(menuSystem.Submenu, "Exit");
            int pressExit = menuItemExit.Command;
            menu.Window.SendCommand(pressExit);
        }

        public void PressTradingDealingRatesManageSymbol()
        {
            WindowMenuItem menuTrading = findSubMenuItemByName(menu, "Trading");
            WindowMenuItem menuItemDealingRates = findSubMenuItemByName(menuTrading.Submenu, "Dealing Rates");
            WindowMenuItem menuManageSymbolSubsription = findSubMenuItemByName(menuItemDealingRates.Submenu, "Manage Symbol Subscription...");
            int pressManageSymbolSubsription = menuManageSymbolSubsription.Command;
            menu.Window.SendCommand(pressManageSymbolSubsription);
        }

        public void TradingOrdersCreateEntryOrder()
        {
            WindowMenuItem menuTrading = findSubMenuItemByName(menu, "Trading");
            WindowMenuItem menuItemOrders = findSubMenuItemByName(menuTrading.Submenu, "Orders");
            WindowMenuItem menuItemCreateEntryOrder = findSubMenuItemByName(menuItemOrders.Submenu, "Create Entry Order...");
            int pressCreateEntryOrder = menuItemCreateEntryOrder.Command;
            menu.Window.SendCommand(pressCreateEntryOrder);
        }
    }
}
