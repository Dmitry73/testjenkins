﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;

namespace TSAutotests
{
    class ApplicationPopupMenuHelper : MenuHelper
    {
        private const string CREATE_CONTINGENT_ORDER_POPUP_MENU = "Create Contingent Order...";
        private const string REMOVE_ORDER_POPUP_MENU = "Remove Order";

        public void openContingentOrdersDialog(TradingStationWindow application)
        {
            using (TradingStationComplexView orders = application.GetOrdersView())
            {
                orders.Grid.SendRightMouseButtonDown(0, 0, InputEventModifiers.None);
                orders.Grid.SendRightMouseButtonUp(0, 0, InputEventModifiers.None);

                Window window = application.GetPopupWindow();
                WindowMenu popupMenu = window.GetMenu();

                WindowMenuItem createContingentOrderItem = findSubMenuItemByName(popupMenu, CREATE_CONTINGENT_ORDER_POPUP_MENU);
                if (createContingentOrderItem != null)
                    application.SendCommand(createContingentOrderItem.Command);
            }
        }

        public bool PressPointPopupMenu(TradingStationWindow application, string namePointMenu)
        {
            Window window = application.GetPopupWindow();
            WindowMenu popupMenu = window.GetMenu();
            WindowMenuItem pressPointMenu = findSubMenuItemByName(popupMenu, namePointMenu);
            if (pressPointMenu.Enabled)
            {
                application.SendCommand(pressPointMenu.Command);
                return true;
            }
            else
            {
                return false;
            }
        }
    }


}
