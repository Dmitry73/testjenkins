﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using System.Collections.Generic;
using System.Threading;

namespace TSAutotests
{
    class OrdersTab : IDisposable
    {
        private const string ORDER_TAB = "Orders";
        private const string REMOVE_ORDER_POPUPMENU = "Remove Order";
        private const int ORDER_ID = 0;
        private const int ORDER_INSTRUMENT = 4;
        private const int ORDER_TYPE = 2;
        private const int ORDER_SELL = 6;
        private const int ORDER_BUY = 7;

        public enum OrderGroups
        {
            OTO,
            OCO,
            OTOCO,
            Entry
        }

        private Dictionary<OrderGroups, string> orderGroupNames = new Dictionary<OrderGroups, string>()
        {
            { OrderGroups.OTO, "OTO" },
            { OrderGroups.OCO, "OCO" },
            { OrderGroups.OTOCO, "OTOCO" },
            { OrderGroups.Entry, "Entry orders" }
        };

        private TradingStationComplexView ordersView;

        public OrdersTab(TradingStationWindow application)
        {
            TradingStationTab ordersTab = null;
            if (application.FindTab(ORDER_TAB) == null)
            {
                var applicationMenu = new ApplicationMenuHelper(application);
                applicationMenu.PressViewOrders();
                Waiter.WaitingCondition condition = () => (application.FindTab(ORDER_TAB)) != null;
                if (!Waiter.Wait(condition))
                    throw new Exception("tab Orders isn't found");
            }
            ordersTab = application.FindTab(ORDER_TAB);
            ordersTab.Click();

            ordersView = application.GetOrdersView();
        }

        public void DeleteAllOrders(TradingStationWindow application)
        {
            var applicationMenu = new ApplicationMenuHelper(application);
            var monitor = new TimeoutMonitor(10 + 10 * GetAllOrdersCount());
            while (GetAllOrdersCount() > 0 && !monitor.checkTimeout())
            {
                int allOrdersCount = GetAllOrdersCount();
                var firstNonEmptyOrdersGroup = FindFirstNonEmptyOrdersGroup();
                firstNonEmptyOrdersGroup.Grid.SendLeftMouseButtonDown(0, 0, InputEventModifiers.None);
                firstNonEmptyOrdersGroup.Grid.SendLeftMouseButtonDown(0, 0, InputEventModifiers.None);
                applicationMenu.PressRemoveOrder();
                YesNoDialog.PressYes("FXCM Trading Station Desktop", application);
                Thread.Sleep(1000);  //HACK
                Waiter.WaitingCondition condition = () => GetAllOrdersCount() != allOrdersCount;
                if(!Waiter.Wait(condition))
                {
                    throw new Exception("Can't close order");
                }
            }
            if (GetAllOrdersCount() > 0)
            {
                throw new Exception("Can't delete all orders");
            }
        }

        private int GetAllOrdersCount()
        {
            int count = 0;
            for (int i = 0; i < ordersView.Grid.Count; i++)
            {
                count += ordersView.Grid.GetGroup(i).Grid.RowCount;
            }
            return count;
        }

        private TradingStationGridGroup.Group FindFirstNonEmptyOrdersGroup()
        {
            for (int i = 0; i < ordersView.Grid.Count; i++)
            {
                var currentGroup = ordersView.Grid.GetGroup(i);
                if (currentGroup.Grid.RowCount > 0)
                {
                    return currentGroup;
                }
            }
            return null;
        }

        public List<String> GetOrdersFromGroup(OrderGroups orderGroupName)
        {
            List<string> ret = new List<string>();
            TradingStationGridGroup.Group orderGroup = FindOrderGroup(orderGroupName);
            if (orderGroup == null)
                return ret;

            for (int j = 0; j < orderGroup.Grid.RowCount; j++)
            {
                ret.Add(orderGroup.Grid.CellValue(j, ORDER_ID));
            }
            return ret;
        }

        private TradingStationGridGroup.Group FindOrderGroup(OrderGroups orderGroupName)
        {
            for (int i = 0; i < ordersView.Grid.Count; i++)
            {
                TradingStationGridGroup.Group orderGroup = ordersView.Grid.GetGroup(i);
                if (orderGroup.Name.IndexOf(orderGroupNames[orderGroupName]) == -1)
                    continue;
                return orderGroup;
            }
            return null;
        }

        public string GetOrderId(List<string> excludeOrders, OrderGroups orderGroupName, string instrument,
            BuySellEnum buySell, double orderRate)
        {
            TradingStationGridGroup.Group orderGroup = FindOrderGroup(orderGroupName);
            if (orderGroup == null)
                return null;
            for (int i = 0; i < orderGroup.Grid.RowCount; i++)
            {
                string orderId = orderGroup.Grid.CellValue(i, ORDER_ID);
                if (excludeOrders.Contains(orderId))
                    continue;
                string orderInstrument = orderGroup.Grid.CellValue(i, ORDER_INSTRUMENT);
                if (!orderInstrument.Equals(instrument))
                    continue;
                string idString = orderGroup.Grid.CellValue(i, ORDER_ID);

                return idString;
            }
            return null;
        }

        public bool CheckOrderExists(string orderId, OrderGroups orderGroupName)
        {
            TradingStationGridGroup.Group orderGroup = FindOrderGroup(orderGroupName);
            if (orderGroup == null)
                return false;
            for (int i = 0; i < orderGroup.Grid.RowCount; i++)
            {
                if (orderGroup.Grid.CellValue(i, ORDER_ID) == orderId)
                {
                    return true;
                }
            }
            return false;
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    ordersView.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
