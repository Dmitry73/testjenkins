﻿using gehtsoft.tscontroller;
using System;
using System.Collections.Generic;

namespace TSAutotests
{
    class SimpleDealingRatesTab : IDisposable
    {
        private const string TAB_TITLE = "Rates";
        private const int SYMBOL_COLUMN = 0;
        private const int SELL_COLUMN = 1;
        private const int BUY_COLUMN = 2;

        private TradingStationSimpleView simpleDealingRatesView;

        public SimpleDealingRatesTab(TradingStationWindow application)
        {
            Waiter.WaitingCondition condition = () => (simpleDealingRatesView = application.FindGridView(TradingStationViewType.Rates)) != null;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("zero value rate in tab Simple Dealing Rates");
            }
        }

        private int FindRow(String instrument)
        {
            for (int i = 0; i < simpleDealingRatesView.Grid.RowCount; i++)
            {
                if (instrument.Equals(simpleDealingRatesView.Grid.CellValue(i, SYMBOL_COLUMN)))
                    return i;
            }
            throw new Exception("No such instrument");
        }

        public double GetRate(string instrument, BuySellEnum buySell)
        {
            int row = FindRow(instrument);

            String sellRateString = simpleDealingRatesView.Grid.CellValue(row, SELL_COLUMN);
            String buyRateString = simpleDealingRatesView.Grid.CellValue(row, BUY_COLUMN);

            double sellRate = Convert.ToDouble(sellRateString);
            double buyRate = Convert.ToDouble(buyRateString);

            return buySell == BuySellEnum.BUY ? buyRate : sellRate;
        }

        public List<double> LogCurrencyPairs(List<string> currencyPairs)
        {
            List<double> lastRecordValue = new List<double>();

            foreach(var instrument in currencyPairs)
            {
                Log.WriteLog(instrument + ":   Sell: " + GetRate(instrument, BuySellEnum.SELL) +
                     "  Buy: " + GetRate(instrument, BuySellEnum.BUY));

            }

            return lastRecordValue;
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    simpleDealingRatesView.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
