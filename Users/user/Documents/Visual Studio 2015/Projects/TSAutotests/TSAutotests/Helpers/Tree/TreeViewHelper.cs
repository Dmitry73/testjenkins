﻿using System;
using gehtsoft.applicationcontroller;

namespace TSAutotests
{
    class TreeViewHelper : IDisposable
    {

        private TreeView treeView = null;
        public TreeViewHelper(Window window, String titleRegex)
        {
            Waiter.WaitingCondition condition = () => window.FindChild("SysTreeView32", titleRegex, true) != null;
            if (!Waiter.Wait(condition, 20))
                throw new Exception("Can't find tree");
            treeView = window.FindChild("SysTreeView32", titleRegex, true) as TreeView;
            if (treeView == null)
                throw new Exception("Something gone wrong");
        }

        //find item title in tree
        public TreeViewItem FindElement(String title)
        {
            TreeViewItem item = treeView.RootItem;
            return Scan(item, title);
        }

        private TreeViewItem Scan(TreeViewItem item, String title)
        {
            while (item != null)
            {
                if (item.Text == title)
                    return item;
                TreeViewItem found = Scan(item.FirstChild, title);
                if (found != null)
                    return found;
                item = item.Next;
            }
            return null;
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    treeView.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
