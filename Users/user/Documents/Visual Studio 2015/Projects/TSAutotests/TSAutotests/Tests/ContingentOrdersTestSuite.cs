﻿using NUnit.Framework;
using gehtsoft.tscontroller;
using System;
using System.Collections.Generic;
using System.Threading;
using gehtsoft.applicationcontroller;
using System.Diagnostics;

namespace TSAutotests
{
    [TestFixture]
    public class ContingentOrdersTestSuite
    {
        #region Actions start and end of testing
        [OneTimeSetUp]
        public void BeginMetod()
        {
            TradingStation.StartApplication();
            Waiter.DisposableWaitCondition condition = () => TradingStation.FindWindow(true);
            if (!Waiter.DisposableWait(condition, 20))
                throw new Exception("Not found window Trading Station"); 
            using (var application = TradingStation.FindWindow(true))
            {
                
                using (var loginDialog = new LoginDialog(application))
                {
                    if(!loginDialog.Login("fxu100d1_dev", "u100d1dev", application))
                    {
                        throw new Exception("Can't login");
                    }
                }
            }
        }

        [OneTimeTearDown]
        public void FinalMetod()
        {
            if (TestContext.CurrentContext.Result.FailCount > 0)
            {
                var collectorLog = new CollectorLog();
                collectorLog.ArchivingLogsFall();
            }

            using (TradingStationWindow application = TradingStation.FindWindow(true))
            {
                //var tsMenu = new ApplicationMenuHelper(application);
                //tsMenu.PressSystemExit();
                //YesNoDialog.PressYes("FXCM Trading Station Desktop", application);
            }
        }
        #endregion
        #region Action before and after each test
        [SetUp]
        public void PerformBeforeEachTest()
        {
            using (TradingStationWindow application = TradingStation.FindWindow(true))
            {
                var pressButtonMenu = new ApplicationMenuHelper(application);
                pressButtonMenu.PressViewDefault();
            }
        }

        [TearDown]
        public void PerformAfterEachTest()
        {

            using (TradingStationWindow application = TradingStation.FindWindow(true))
            {
                var pressButtonMenu = new ApplicationMenuHelper(application);
                pressButtonMenu.PressViewDefault();
            }
        }
        #endregion

        private List<string> currencyPairs = new List<string>() { "EUR/USD", "AUD/USD" };

        public void PreparationTestOtoOrder()
        {
            using (TradingStationWindow application = TradingStation.FindWindow(true))
            {
                using (var ordersTab = new OrdersTab(application))
                {
                    ordersTab.DeleteAllOrders(application);
                    var openPositionsTab = new OpenPositionsTab(application);
                    var applicationMenu = new ApplicationMenuHelper(application);
                    if (openPositionsTab.OpenPositionsCount > 0)
                    {
                        applicationMenu.PressCloseAllPositions();
                        using (var closeAllPositionsDialog = new CloseAllPositionsDialog(application))
                        {
                            closeAllPositionsDialog.PressOK();
                        }
                    }
                    var manageSymbolDialog = new ManageSymbolSubscriptionDialog(application);
                    manageSymbolDialog.leaveOnlyThoseCurrenctyPairs(currencyPairs);
                }
            }
        }

        public string[] CreateOtoOrder(TradingStationWindow application)
        {
            using (var ordersTab = new OrdersTab(application))
            {
                List<string> existingOrders = ordersTab.GetOrdersFromGroup(OrdersTab.OrderGroups.OTO);
                using (var ratesTab = new SimpleDealingRatesTab(application))
                {
                    double delta = 5;
                    double pip = 0.00001;

                    double exhibitionPrimaryRate = ratesTab.GetRate(currencyPairs[0], BuySellEnum.BUY) + delta * pip;
                    double exhibitionSecondaryRate = ratesTab.GetRate(currencyPairs[1], BuySellEnum.SELL) + delta * pip;

                    using (var createOtoOrderDialog = new CreateContingentOrderDialog(application))
                    {
                        createOtoOrderDialog.CreateOtoOrder(currencyPairs[0], exhibitionPrimaryRate,
                            currencyPairs[1], exhibitionSecondaryRate);
                    }

                    string primaryOrderId = ordersTab.GetOrderId(existingOrders, OrdersTab.OrderGroups.OTO,
                        currencyPairs[0], BuySellEnum.BUY, exhibitionPrimaryRate);
                    string secondaryOrderId = ordersTab.GetOrderId(existingOrders, OrdersTab.OrderGroups.OTO,
                        currencyPairs[1], BuySellEnum.SELL, exhibitionSecondaryRate);
                    string[] idOrderCurrencyPair = new string[] { primaryOrderId, secondaryOrderId,
                        exhibitionPrimaryRate.ToString(), exhibitionSecondaryRate.ToString()};

                    if (idOrderCurrencyPair[0]==null)
                        throw new Exception("could not create OTO order");
                    return idOrderCurrencyPair;
                }
            }
        }

        [Test]
        public void TestOtoOrder()
        {
            PreparationTestOtoOrder();
            using (TradingStationWindow application = TradingStation.FindWindow(true))
            {
                string[] orderIdFirst = CreateOtoOrder(application);

                string priceExecutionFirstOrder = orderIdFirst[2];
                string priceExecutionSecondOrder = orderIdFirst[3];

                List<string> currencyPairOnlySecond = new List<string>();
                currencyPairOnlySecond.Add(currencyPairs[1]);

                using (var orderTab = new OrdersTab((TradingStationWindow)application))
                {
                    using (var dealingRates = new SimpleDealingRatesTab((TradingStationWindow)application))
                    {
                        var timeoutMonitor = new TimeoutMonitor(120);
                        bool checkAdequacyPrice = false;
                        double lastPrimaryRate = 0;
                        double lastSecondaryRate = 0;
                        while (!timeoutMonitor.checkTimeout())
                        {
                            if (orderTab.CheckOrderExists(orderIdFirst[0], OrdersTab.OrderGroups.OTO))
                            {
                                dealingRates.LogCurrencyPairs(currencyPairs);
                                lastPrimaryRate = dealingRates.GetRate(currencyPairs[0], BuySellEnum.BUY);
                                lastSecondaryRate = dealingRates.GetRate(currencyPairs[1], BuySellEnum.SELL);
                            }
                            else
                            {
                                //value for money (checking whether the price went across the border)
                                //if the value is positive, the border crossing was done
                                checkAdequacyPrice = (dealingRates.GetRate(currencyPairs[0], BuySellEnum.BUY) -
                                    double.Parse(priceExecutionFirstOrder)) *
                                    (lastPrimaryRate - double.Parse(priceExecutionFirstOrder)) <= 0;

                                Log.WriteLog("order with the first currency pair " + currencyPairs[0] +
                                    " load \r\n opening price orders to " + BuySellEnum.BUY+ " was exhibited - " + priceExecutionFirstOrder +
                                    "\r\n order is triggered by the price - " + dealingRates.GetRate(currencyPairs[0], BuySellEnum.BUY));
                                break;
                            }
                            Thread.Sleep(500);
                        }
                        Assert.False(orderTab.CheckOrderExists(orderIdFirst[0], OrdersTab.OrderGroups.OTO));
                        Assert.True(checkAdequacyPrice);

                        timeoutMonitor = new TimeoutMonitor(120);
                        while (!timeoutMonitor.checkTimeout())
                        {
                            if (orderTab.CheckOrderExists(orderIdFirst[0], OrdersTab.OrderGroups.Entry))
                            {
                                dealingRates.LogCurrencyPairs(currencyPairOnlySecond);
                                lastSecondaryRate = dealingRates.GetRate(currencyPairs[1], BuySellEnum.SELL);
                            }
                            else
                            {
                                Trace.WriteLine("order with the second currency pair " + currencyPairs[1] +
                                    " load \r\n opening price orders to " + BuySellEnum.SELL + " was exhibited - " + priceExecutionSecondOrder +
                                    "\r\n order is triggered by the price - " + dealingRates.GetRate(currencyPairs[1], BuySellEnum.SELL));
                                //Log.WriteLog("order with the second currency pair " + currencyPairs[1] +
                                //    " load \r\n opening price orders to " + BuySellEnum.SELL + " was exhibited - " + priceExecutionSecondOrder +
                                //    "\r\n order is triggered by the price - " + dealingRates.GetRate(currencyPairs[1], BuySellEnum.SELL));
                                break;
                            }
                            Thread.Sleep(500);
                        }
                        Assert.False(orderTab.CheckOrderExists(orderIdFirst[1], OrdersTab.OrderGroups.Entry));
                        checkAdequacyPrice = (dealingRates.GetRate(currencyPairs[1], BuySellEnum.SELL) -
                            double.Parse(priceExecutionSecondOrder)) *
                            (lastSecondaryRate - double.Parse(priceExecutionSecondOrder)) <= 0;
                        //Assert.True(checkAdequacyPrice);
                    }
                }

            }
        }
    }
}
